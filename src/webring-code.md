<style>
:root {
  --wwwebring-background: transparent;
  --wwwebring-border: none;
}

td, th {
  background: #fff;
  padding: 0.25em 0.5em;
  border: 1px solid #ccc;
}
</style>

# Webring Code

This page assumes you have already [asked to be added](/join.html) (or have been invited) to the webring.

To link yourself to other sites in the Wayward Webring, do the following:

**Step 1:** Download [this JavaScript file](/wwwebring.min.js) (`wwwebring.min.js`) and upload it to your site.

**Step 2:** Add the HTML below to your site, where you want the webring widget to appear. Replace the `https://yoursite.example.com/wwwebring.min.js` URL with the actual location of `wwwebring.min.js` on your site.

```html
<script defer src="https://yoursite.example.com/wwwebring.min.js"></script>
<div
  data-wwwebring="https://waywardweb.org/ring.json"
  data-wwwebring-theme="default"
></div>
```

And that's it! That's all you need to do. The widget should look approximately like this if it's working:

<script defer src="/wwwebring.min.js"></script>
<div
  data-wwwebring="https://waywardweb.org/ring.json"
  data-wwwebring-theme="default"
></div>

If you have questions or concerns, the sections below might address them.

## Custom styling

<details>
<summary>Show/hide</summary>

If you want maximum control over how the widget looks, see the section below, about the [JavaScript-free](#links-only-version-javascript-free) way to link yourself into the webring.

If you just want to change the widget's background color, font color, or border, you can do that via CSS variables. The available variables are:

- `--wwwebring-border` - applied to the border property of the widget. Example value: `2px solid black`.
- `--wwwebring-background` - applied to the background property. Example value: `url(https://placekitten.com/200/300)`.
- `--wwwebring-text-color` - applied to the color property. Example value: `green`.

To set these variables, you can add them to a style attribute on the widget div:

```html
<div
  style="--wwwebring-text-color: green; --wwwebring-background: #eff;"
  data-wwwebring="https://waywardweb.org/ring.json"
  data-wwwebring-theme="default"
></div>
```

</details>

## Links-only version (JavaScript free!)

<details>
<summary>Show/hide</summary>

The code above might not work for you, for a number of reasons:

- Your web host might not allow JavaScript, or might not allow your pages to fetch data from other sites (e.g. `https://waywardweb.org/ring.json`).
- Your site's visitors might have JavaScript disabled.

You also might simply dislike having a big ugly webring widget on your page. Fair enough. I'm not a huge fan of how it looks myself.

If any of these applies to you, you can use links like the following instead (replacing `yoursite.example.com` with the actual domain of your site):

```html
<a href="https://waywardweb.org/sites/yoursite.example.com/previous">&larr<!-- left arrow --></a>
<a href="https://waywardweb.org">The Wayward Webring</a>
<a href="https://waywardweb.org/sites/yoursite.example.com/next">&rarr;<!-- right arrow --></a>
```

Here's what the result will look like:

<a href="https://waywardweb.org/sites/waywardweb.org/previous">&larr;<!-- left arrow --></a>
<a href="https://waywardweb.org">The Wayward Webring</a>
<a href="https://waywardweb.org/sites/waywardweb.org/next">&rarr;<!-- right arrow --></a>

Of course, you can use any variant of this. It will work as long as you have links to these URLs:

- `https://waywardweb.org/sites/yoursite.example.com/previous`
- `https://waywardweb.org/sites/yoursite.example.com/next`
- `https://waywardweb.org`

### Pros and cons of the two approaches

Here are some things to consider when deciding whether to use JavaScript or links for your webring widget:

| Feature | JavaScript | Links |
| ------: | ---------- | ----- |
| Updates automatically when members join or leave the ring | ✅ | ✅ |
| Best for screenreader users | ✅ | ❌ |
| Users can see site names &amp; URLs before navigating | ✅ | ❌ |
| You can try it out before I add you to the ring | ✅ | ❌ |
| Works with JS disabled | ❌ | ✅ |
| Can be styled however you want | ❌ | ✅ |

</details>

## Mirrors and dev servers

<details>
<summary>Show/hide</summary>

The JavaScript webring widget looks at the current URL of the page to figure out which site it's on and thus where the "previous" and "next" links should go. If you have a mirror of your main site, or a local development server, and you want the widget to work there, you'll have to do a tiny bit of extra configuration.

Add a `data-wwwebring-you-are-here` attribute to your HTML, like this:

```
<div
  data-wwwebring="https://waywardweb.org/ring.json"
  data-wwwebring-theme="default"
  data-wwwebring-you-are-here="https://yoursite.example.com"
></div>
```

Replace `https://yoursite.example.com` with the URL of your main site.

If the widget can't figure out which site it's on based on the real URL of the page, it will look at the `data-wwwebring-you-are-here` hint instead.

</details>