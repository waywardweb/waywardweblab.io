# Why This Webring Exists

_tl;dr I think we need a viable alternative to Google and ChatGPT if we're to have a healthy information ecosystem. The Wayward Web is the best idea I could come up with in two weeks of ruminating._

The Wayward Web is essentially a [webring](https://ring.recurse.com/) for websites that link to a bunch of other websites. Where's the fun in that?

The point of the webring is to start a community of **web curators**. We don't just to link to any old websites, we link to sites we think are *good*. Sites we love, sites we use, sites that are true treasures. Unearthing these gems from the muck of the wider internet and sharing them with others adds real value to the world.

**Curation** is a four-step process:

- curation =
  - searching
  - evaluating
  - collecting
  - presenting

(there's actually one more step, _preserving_, but that's out of scope for this post. I might write something on [archiving](archiving.html) in the future — if I do, that link will go somewhere other than a 404 page.)

The first half of the curation process is what we all do when we're browsing the internet. We all search for websites and evaluate whether we like them and trust them. But we usually don't complete the process: it's rare that we collect and present what we find.

Every unfinished act of curation is a tiny tragedy: knowledge that could have been shared and preserved, but was siloed or lost instead. Our constant use of the Web means that within every one of us, there is an immense, un&shy;tapped store of knowledge that could be made public, findable, and durable.

The purpose of the Wayward Web is to unlock that store of knowledge.

<!--

As a starting point, I recommend reading  Cameron Desautels' post ["Curation is Creation"](https://camdez.com/blog/2016/03/30/curation-is-creation/) (1200 words, 6 minute read).

Here's the takeaway quote:

> Let me ask a pointed question—who contributes more to the human enterprise:
>
> - Someone who writes a great book no one ever reads?
> - Or someone who leads millions of others to an unknown, life-changing book?
>
> [...] In this era, new music comes out at such a rate that no one can keep up with it (let alone know the existing catalog!). As of 2012, an hour of YouTube footage was uploaded every second. In the U.S. alone, 300,000+ books are published every year.
>
> Obviously these numbers are dramatically higher than anyone can consume, yet great works are worthless unless discovered, so curation is just as critical as traditional creation.

-->

## Why Curation Matters on the Modern Web

Our culture is constrained by our experiences. Our experiences are constrained by _what we can find_ — what we know about, what's nearby, what's accessible to us.

If we want to create a vibrant culture, then **web search,** i.e. Google, is a problem. Two rea&shy;sons:

- To search for something, you have to know what it's called. This makes it hard to discover truly new things.
- Google has an incentive to steer you toward commercial, capitalist sites, and away from knowledge and cultural experiences that you might find more empowering or fulfilling.

### The Decline of Search

Web search was never a perfect solution, but for a couple decades it worked pretty well. Search engines were incentivized to send people to trustworthy sites, and their ranking systems were hard enough to game that spam didn't proliferate. Recent trends — specifically, generative AI and the rise of copycat spam sites — have changed all of that.

Generative AI makes the web shittier in at least two ways:

- GenAI **steals information** from websites and regurgitates it (or a garbled form of it) in a chat interface — sometimes with attribution, but often not. In any case, chatbots steer people away from interacting with the _actual websites_ from which their training data derives, making it harder for the creators of those websites to reap the financial or reputational rewards of having people visit their page.

- GenAI makes it easy to generate enormous quantities of **bullshit text, images, and videos** that are hard to distinguish from the real thing. Unless search engines figure out a way to filter out AI-generated content (and see an incentive for doing so) we can expect the human signal of the web to be drowned out by the noise of robot effluent within the next few years.

The upshot is that we can no longer rely on automated methods to help us find information on the web. The path forward, therefore, must be one of human connection.

### What About Web Directories?

Before there was search, there were [web directories](https://en.wikipedia.org/wiki/Web_directory) — giant lists of websites run by the likes of Yahoo!. These web directories worked, but they were a poor substitute for real curation. They were impersonal, and like the search engines of today, mostly commercial. Because they tried to be comprehensive and not to editorialize, it was up to the user to separate the wheat from the chaff.

This is why I'm not advocating for a return to centralized web directories — directories like Yahoo! and [vlib.org](http://vlib.org) that pretend to be comprehensive and neutral. Neutrality is an illusion; it can't exist, so we shouldn't pretend that it exists. I think we can do better with a decentralized approach, where people build their own networks of trust based on real-life personal connections and the gradual accretion of experiences with various sites.

## The Web Needs Many, Connected Gates

When access to the web is centralized through a single portal — whether it's a search engine or a directory — the owners of that portal have too much power over what people can find. We as individual web users can't do much to capture mindshare from giant corporations, but we don't have to remain individuals. We can do together what we cannot do alone.

This is why I want to create a webring, rather than just a personal website. Yes, I could just curate a list of my favorite sites, and that list might even be useful to a few other people, but so what? It wouldn't make a dent, compared to the vast scale of the problems with the modern web.

The solution is to decentralize: many gates to the web, instead of just one or a few. A vibrant web needs many curators. If those curators link to the pages of other curators whose opinions they trust, the whole system starts to look very much like a comprehensive web directory, only no one person holds the keys to the kingdom, and many different viewpoints are represented.

A potential criticism: the webring itself is a form of centralization. Though I acknowledge that this is a risk, I see the webring as merely a catalyst, a way of getting the process of interlinking started. My hope is that as curators discover and link to each others' pages, the webring _per se_ will gradually make itself obsolete.

## A Vision of the Future

How would it feel to use this future, curated Web?

Imagine you've found the sites of 150 or so ([Dunbar's number](https://en.wikipedia.org/wiki/Dunbar%27s_number)) web curators. Each of their sites focuses on a few different topics, so you know who to go to if you want, say, cookware recommendations, or explanations of chess strategy, or early-2000s Flash videos. Some of their websites might let you subscribe to updates, whether through email or RSS or ActivityPub, and through these updates you're constantly discovering new ideas and things — wacky, crazy, cool things that you _never_ would have thought to search for. Maybe you have a webpage where you list your favorites, and maybe some of the curators you link to link back.

It would basically be a social network, but without the doomscrolling.

Doesn't that sound nice?

## Further Reading

- [A Guide to Web Curation](how.html) - how to select and present a great link collection.
- [Cameron Desautels, "Curation is Creation"](https://camdez.com/blog/2016/03/30/curation-is-creation/) - a major inspiration
- [Every Site Needs a Links Page](https://thoughts.melonking.net/thoughts/every-site-needs-a-links-page-why-linking-matters) by [Melon](https://melonking.net/)