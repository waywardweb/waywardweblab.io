<style>
  nav.phat { display: none }

  li {
    font-style: italic;
    font-size: 0.9rem;
    color: #444;
    word-spacing: 0.1em;
    text-align: left;
  }

  li a {
    font-style: normal;
    font-size: 1rem;
  }

  ul > li > ul > li {
    display: inline;
    font-style: italic;
    font-size: 0.9rem;
    color: #444;
    word-spacing: 0.1em;
    list-style: none;
  }

  /* ul > li > ul {
    display: inline;
    margin: 0;
    padding: 0;
  } */

  ul > li > ul > li:not(:first-child)::before {
    content: '• ';
    padding-inline: 0.25em;
  }
</style>

# The Wayward Webring

<center-text>

a collection of link collections!

[Why This Site Exists](why.html)

[Join the Webring!](join.html)

</center-text>

## Link Collections by Our Members

- [**Ben Christel**](https://benchristel.com/portal.html)
  - Web dev
  - One-pot vegetarian meals
  - Meditation
  - Tech alternatives
  - Computer habitability
  - Constructed languages
  - Public-domain images, books, &amp; data
- [**Culture Machine**](https://tv.benchristel.com/)
  - Calm YouTube on shuffle
  - Concerts
  - Theatre
  - Mythology
  - Linguistics
  - Science
  - History
  - Gardening
  - Nature videography
- [**Brandon&rsquo;s Journal**](https://brandons-journal.com/links/)
  - Personal blogs
  - Small web
  - Fediverse
  - History
  - Retro and nostalgia
  - Comic books
  - Fan sites
  - Buddhism and stoicism
  - Horror movies
  - Westerns
  - Swords and sorcery
  - Urban exploration
  - Message boards
- [**EGGRAMEN**](https://eggramen.neocities.org/elsewhere/links)
  - Website templates
  - Conlanging
  - Interactive fiction
  - Golden Sun
  - Homestuck
  - Zelda
  - Filk
  - Neocities sites
  - Small web
  - TTRPGs
  - Other
  - Webcomics
  - Webrings
- [**Mike Grindle**](https://mikegrindle.com/blogroll/)
  - Search engines
  - Images
  - Libraries
  - News
  - Radio
  - Tech
  - Personal sites
- [**imladris**](https://braigwen.neocities.org/linklist)
  - Disability and accessibility
  - Life skills
  - Media access
  - Writing
  - Webmastery
  - Video essays
- [**Tom&rsquo;s Homepage**](https://ttntm.me/bookmarks/)
  - Blogs
  - Entertainment
  - Research
  - Tools &amp; utilities
  - Web directories
  - Website inspiration
- [**foreverliketh.is**](https://foreverliketh.is/)
  - Education
  - Tech lite
  - Life
  - Psychology
  - Journaling
- [**Skyhold**](https://skyhold.org/links/)
  - Small web
  - Solarpunk
  - Tech alternatives
  - Otherkin
  - Art
- [**Daryl Sun's Journal**](https://blog.darylsun.page/exits/links)
  - Privacy, security, and open source
  - Fediverse
  - Personal web
  - Fandom
  - Productivity and organization
  - Web crafting
  - Media
  - Typography
  - General tools
- [**Melon's Fav Hyperlinks**](https://melonking.net/links)
  - Eclectic YouTube
  - Free electronic music
  - Webmaster tools
  - Radio &amp; TV
  - Retro tech
  - Adventure locations
- [**Terrible Lizard**](https://terriblelizard.info/links)
  - Open-access books and journal articles
  - Online privacy
  - Web crafting
  - Old-school gifs and graphics
- [**Ray's Miscellany**](https://brisray.com/web/webring-list.htm)
  - A database of indie webrings totaling thousands of member sites!

## Pages on This Site

- [Why This Webring Exists](why.html)
- [A Guide to Web Curation](how.html)
- [How to Join the Wayward Web](join.html)
- [Wayward Webring Rules](rules.html)
- [Webring Code](webring-code.html) - now with a JavaScript-free option!

<script defer src="/wwwebring.min.js"></script>
<div
  data-wwwebring="/ring.json"
  data-wwwebring-theme="default"
></div>

<!--
TO INVITE:
amriel.neocities.org
averylychee.neocities.org
chriswere.wales
cidoku.net
-->