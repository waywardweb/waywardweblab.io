# Wayward Webring Rules

As the webring maintainer, I reserve the right to decline to add any site to the webring for any reason. However, for the safety of all involved, and to set expectations, it probably makes sense to have some explicit rules for the ring. Here are the rules.

- **No hate speech** or links to hate speech. No advocating violence against any person or group.

- **Nothing 18+/NSFW visible from the landing page,** and labeled as such if you link to it.

That's it!

Credit for these rules goes to [Em Reed](https://emreed.net/) of the [Low Tech Webring](https://emreed.net/LowTech_Directory).