# A Guide to Web Curation

<center-text>

how to actually do it.

</center-text>

Lots of websites are already curating various corners of the web. Some do it well; others, not so well. We can learn from the good examples as well as the bad. In this guide, I hope to explain what makes the successful examples of web curation work.

## Good Examples

Let's start with some examples of curation done well.

### [Richard Kennaway's List of Constructed Languages](http://www2.cmp.uea.ac.uk/~jrk/conlang.html)

A massive, annotated list of hundreds of conlangs.

The thing I love most about this page is how much detail it gives about each language. Most of the links have an example quote in the language and its translation into English. The main weakness of this page is that it's unmaintained, and many of the links are dead now, but that almost doesn't matter. The amount of detail makes the list a fascinating artifact in its own right.

### [Ardalambion](http://ardalambion.org) 

Ardalambion contains both original research into J.R.R. Tolkien's constructed languages, and links to other sites. Each link has a concise description and the site has a distinctive visual style. I love Ardalambion for its personality and its focus. It's a site entirely dedicated to Tolkien's languages, and if you go there, you know what you're getting.

### [Zompist's Web Resources for Language Construction](https://zompist.com/resources/index.html)

Another website about constructed languages!? Yeah, there's a pattern here. A well-annot&shy;ated and well-organized set of resources. Note that Zompist links to Richard Kennaway's page.

### [Ruby Klover's Bookmarks](https://www.bruh.ltd/lists/bookmarks/)

Finally, a site that isn't about conlangs. There are no annotations here, but it's an interesting set of links nonetheless. There's also a [list of lists](https://www.bruh.ltd/lists/) which includes such useful things as a [cookbook](https://www.bruh.ltd/lists/cookbook/) and a [shopping list](https://www.bruh.ltd/lists/consumables/) (or maybe a list of product recommendations - good to have if you're ever in Russia and need to buy shampoo).

## What These Examples Have In Common

A good link collection is more than a pile of URLs. It's organized. It has purpose and personality. And it gives readers some reason to trust or at least like it.

## How Curation Fails

Now let's look at how web curation goes awry. Curation fails when it's...

- **Too democratized**. [The point of curation](why.html) is to make something better than the current systems of search and hyperlinking can achieve. Simply taking the average of what everyone else thinks runs counter to that. Example of what not to do: [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted), a list of links that, while otherwise pretty good, suffers from being the set union of 1260 people’s opinions.

- **Devoid of personality**. Out of the billions of opinions on the web, why should readers trust yours? If your site looks and sounds like everyone else’s, no one will be able to tell how cool you are. Example of what not to do: the [Data Science Trello Board](https://trello.com/b/rbpEfMld/data-science), which describes itself as a collection of "great data science materials" but doesn't give the reader enough information to assess that claim quickly.

- **Lacking annotations**. Why should anyone click these links? Readers need more than a wall of blue, underlined text to hold their interest. Examples of what not to do: my own site [Bastion](https://bastionhome.github.io/), and [ytoo.org](https://ytoo.org/) (both great sites in other respects, if I do say so myself).

- **Just a list of links to other lists of links**. Don't _only_ link to other curators' pages. If you like some of their links, incorporate those into your own collection; don't make the reader go diving for them. Also, avoid creating deep navigation hierarchies within your own site. They’re disorienting and annoying to traverse. Extreme example: [arkmsworld](https://arkmsworld.neocities.org), which buries its link collections in what is essentially a text adventure game made of HTML. I love arkmsworld for what it is — a wonderful place to wander and get lost — but I don't go there expecting to be able to find something specific.

- **Shoehorned into a one-size-fits-all classification scheme**. We don’t need the Dewey Decimal System for websites, IMO. In order for personal websites to feel personal, control over their organization must be local and fit to context. Curators can organize their links in whatever way makes sense to them and their readers. Example: [vlib.org](https://vlib.org/), one of Tim Berners-Lee's sites, and possibly the oldest directory on the web, tries too hard to be a digital library. By aiming to be comprehensive and "neutral," it sets itself up for failure.

## Patterns for Curators

Finally, we can distill some general guidelines from these examples. Here are my suggestions for how to make your collection of links stand out and live a long, prosperous life.

- **Own a domain name**. This lets you change hosting providers if you ever need to.
- **Style your website**. And write in a recognizable voice. Let your readers know it’s you. The medium can be the message here, and a little personality goes a long way.<!--Examples:
  [Ardalambion](http://ardalambion.org), [zompist.com](https://zompist.com), [Ruby Klover](https://www.bruh.ltd), [ytoo.org](https://ytoo.org), [Bastion](https://bastionhome.github.io), [dreamsongs.com](https://dreamsongs.com), [rialian.com](https://arkaia.gitlab.io/http/s/rialian.com/)-->
- **Retain editorial control**. A curator page is the opposite of an anarchic wiki. If you solicit submissions from others, be prepared to reject a lot of them.
- **Link mostly to “leaf” sites** — that is, sites that can’t use these curation patterns, because they are a) collaborative/social, b) applications/tools, c) reference works, or d) focused on original work.
- **List your own projects.** Don't neglect your own original work! Example: Richard Gabriel’s [dreamsongs.com](https://dreamsongs.com).
- **Link to sites you use and love**. The surest indicator of quality is that you keep coming back, and you feel good about doing so.
- **Link to sites that will remain relevant**. Some of my favorite curator pages have links that are decades old. Such pages stay useful even when unmaintained. By contrast, trying to curate the news or the latest memes is a Sisyphean task, and basically guarantees that your page will forever be out of date.
- **Link to other curators’ pages in a section reserved for that purpose**. Links to other lists should be separate from links to actual content. Exception: hat-tip links (below).
- If you got a link from another curator’s page, **give them a hat tip (h/t)** for discovering it, and link to them inline. Example:
  > - [John Cleese on Creativity in Management](https://www.youtube.com/watch?v=Pb5oIIPO62g) (video, 37 minutes) - the Monty Python cofounder discusses how to cultivate an open and playful mind (h/t [Dani](https://bookmarkbeat.substack.com/p/bookmark-beat-ep-20))
- **Annotate each link** with a brief description, and (if appropriate) an evocative quote or image. The gold standard for this is [Richard Kennaway’s conlang list](http://www2.cmp.uea.ac.uk/~jrk/conlang.html). But your annotations need not contain that much detail; [ardalambion.org](http://ardalambion.org) is a great example of a more lightweight style.

  The harder it is for readers to evaluate the content behind each link, the more infor&shy;mation you should give them upfront. E.g. if you're listing software tools, consider providing screenshots, system requirements, and price alongside each link. [Richard Gabriel annotates large PDF downloads with their file size](https://dreamsongs.com/Essays.html) — presumably so people on slow connections can avoid those links.
- **Embed content** directly in your page where possible and appropriate — e.g. you can embed YouTube videos so your readers don't have to click through to another page.
- **Check for dead links** periodically, and, if you find one, replace it with an archive (I am working on finding/writing tools to automate these tasks).

And finally…

- **Break any of these “rules”** whenever you want. It's your site, after all.

Happy linking!

—[Ben](https://benchristel.com)