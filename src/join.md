# How to Join the Wayward Web

<center-text>

You can be a part of the Wayward Web! This page describes how to join.

</center-text>

## Step 1

First, you may want to read:

- [Why This Webring Exists](why.html)
- [The rules](rules.html)
- [The how-to guide for web curators](how.html)

If those pages pique your interest, proceed to...

## Step 2

You'll need a webpage (obviously) where you maintain a curated collection of links or other info. We'll call this page your **curation page**.

If you don't have a website yet, [Neocities](https://neocities.org) is a great, free indie webhost.

Please actually have some stuff on your curation page before proceeding to the next step ;). I don't really want to maintain a webring full of placeholder pages...

## Step 3

Tell me about your website via [this Google form](https://docs.google.com/forms/d/e/1FAIpQLScsy0R3xCz6dcKOcF0_AQn8zuG3rEizQnKfzERgvNfB3BjGiA/viewform).

You can also email me (Ben) at [ben.christel@gmail.com](mailto:ben.christel@gmail.com). Be sure to include:

- the URL of your curation page
- the name of your site (if it isn't obvious)

This does not guarantee that I will add you to the webring. I might decline to add your site because it doesn't fit the webring's [theme](/why.html), or doesn't follow the [rules](/rules.html), or because I just don't feel like it.

## Step 4

Add the [webring widget code](webring-code.html) to your curation page!